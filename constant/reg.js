module.exports = {
    doctype: /<!DOCTYPE ([a-z\-]+)(?:\s*([\S\s]*))>/im,
    htmlTag: /<html\s*[\S\s]*>/i,
    elementTag: /<([a-z:\-]+)(?:\s*([^\/]*))(\/?)>/im,
    attribute: /([a-z:\-]+)(?:=((?:'[^']*')|(?:"[^"]*")))?/g
}