const TextNode = require("./core/textNode")
const CommentNode = require("./core/commentNode")
const AttributeNode = require("./core/attributeNode")
const DocumentTypeNode = require("./core/documentTypeNode")
exports.TextNode = TextNode
exports.CommentNode = CommentNode;
exports.AttributeNode = AttributeNode;
exports.DocumentTypeNode = DocumentTypeNode;