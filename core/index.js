const { doctype, htmlTag } = require("../constant/reg");
const DocumentNode = require("./documentNode");
const DocumentTypeNode = require("./documentTypeNode");
const moment = require('moment');
const TextNode = require("./textNode");
module.exports = class parseHTML extends DocumentNode {

    /**
     * 当前处理文本的位置
     */
    positionIndex = -1;

    /**
     * 当前处理文本的所在行数
     */
    rowIndex = -1;

    /**
     * 当前处理文本的所在行列数
     */
    lineIndex = -1;

    /**
     * 要处理的文本总长度
     */
    contentLength = -1;

    /**
     * 处理文本耗时
     */
    handleTime = -1

    constructor(htmlStr) {
        super();
        this.positionIndex = 0;
        this.rowIndex = 0;
        this.lineIndex = 0;
        const beginTime = moment()
        console.log(`[${beginTime.format("yyyy-MM-DD HH:mm:ss")}]开始解析文本`)
        this.htmlStr = htmlStr;
        this.contentLength = this.htmlStr.length;
        console.log(`[${moment().format("yyyy-MM-DD HH:mm:ss")}] 获取文档申明类型`)
        this.getDocumentType();
        console.log(`[${moment().format("yyyy-MM-DD HH:mm:ss")}] 处理文档`)
        this.parseRoot()
        const endTime = moment();
        console.log(`[${beginTime.format("yyyy-MM-DD HH:mm:ss")}]结束解析文本`)
        this.handleTime = endTime.diff(beginTime, 'milliseconds')
        console.log(`[耗时] =>${this.handleTime}ms [文本长度]=>${this.contentLength}`)
    }

    /**
     * 获取文档的类型
     */
    getDocumentType() {
        const result = this.htmlStr.match(doctype);
        if (result && result.length) {
            this.doctype = new DocumentTypeNode(result[1], result[2]);
            this.seek(result[0])
        } else {
            this.doctype = null;
        }
    }

    /**
     * 解析根节点
     */
    parseRoot() {
        if (this.doctype) {
            switch (this.doctype.nodeName) {
                case 'html':
                    const result = this.htmlStr.trim().match(htmlTag);
                    if(result && result.index === 0){
                        this.root = new DocumentNode(this.htmlStr.slice(this.positionIndex + 1));
                    } else {
                        console.warn("文档申明与文档格式无法对应")
                        this.root = null; 
                    }
                    break;
                default:
                    console.warn("未知的文档申明,请扩展功能以解析文档")
                    this.root = null;
                    break;
            }
        } else {
            this.root = new TextNode(this.htmlStr);
        }
    }

    /**
     * 根据内容更新处理的位置和行数
     * @param {String} content 
     */
    seek(content) {
        this.positionIndex += content.length - 1;
        this.rowIndex += content.split(/(\r)?\n/).length - 1;
    }
}