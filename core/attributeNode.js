const { NODETYPE } = require("../constant/node")
const BaseNode = require("./baseNode")

module.exports = class AttributeNode extends BaseNode {
    constructor(key, value) {
        super(NODETYPE.ATTRIBUTE_NODE);
        this.name = key;
        if (this.key === 'class') {
            this.value = value ? value.split(" ") : "";
        }
    }

    get localName() {
        if (this.name) {
            return this.name.split(":").slice(-1)[0]
        }
        return null
    }

    get prefix() {
        if (this.name) {
            return this.name.slice(0, - this.localName.length - 1)
        }
        return null
    }

    isEqualNode(otherNode) {
        return otherNode &&
            this.nodeType === otherNode.nodeType &&
            this.value !== otherNode.value &&
            this.name !== otherNode.name
    }
}