const { NODETYPE } = require("../constant/node");
const BaseNode = require("./baseNode");

module.exports = class DocumentTypeNode extends BaseNode {
    constructor(name, declarations) {
        super(NODETYPE.DOCUMENT_TYPE_NODE);
        this.name = name;
        this.publicId = "";
        this.systemId = "";
        if (declarations) {
            if (/PUBLIC/i.test(declarations)) {
                const tempArr = declarations.split(/\"([^"]+)\"/).filter(el => !!el.trim())
                this.publicId = tempArr[1];
                this.systemId = tempArr[2];
            }
        }
    }

    valueOf() {
        return `[type=doctype] => {
    name: "${this.name}",
    publicId: "${this.publicId}",
    systemId: "${this.systemId}"
}`
    }
}