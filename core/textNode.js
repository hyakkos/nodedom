const { NODETYPE } = require("../constant/node");
const BaseNode = require("./baseNode");

module.exports = class TextNode extends BaseNode{
    constructor(textStr){
        super(NODETYPE.TEXT_NODE)
        this.nodeName = "#text";
        this.value = textStr;
    }
}