const { NODETYPE } = require("../constant/node");

module.exports = class BaseNode {

    _parentNode = null;

    constructor(nodeType) {
        this.nodeType = nodeType;
    }

    get childNodes() {
        return []
    }

    get firstChild() {
        if (this.hasChildNodes()) {
            return this.childNodes[0]
        } else {
            return null;
        }
    }

    get lastChild() {
        if (this.hasChildNodes()) {
            return this.childNodes.slice(-1)[0]
        } else {
            return null;
        }
    }

    get nextSibling() {
        if (this.parentNode && this.parentNode.hasChildNodes()) {
            const index = this.parentNode.childNodes.findIndex(el => el.isEqualNode(this));
            if (index !== this.parentNode.childNodes.length - 1) {
                return this.parentNode.childNodes[index + 1];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    get previousSibling() {
        if (this.parentNode && this.parentNode.hasChildNodes()) {
            const index = this.parentNode.childNodes.findIndex(el => el.isEqualNode(this));
            if (index === 0) {
                return null;
            } else {
                return this.parentNode.childNodes[index - 1]
            }
        } else {
            return null;
        }
    }

    get parentNode() {
        return this._parentNode
    }

    set parentNode(val) {
        this._parentNode = val;
    }

    hasChildNodes() {
        return this.childNodes.length !== 0
    }

    contains(otherNode) {
        if (this.isEqualNode(otherNode)) {
            return true;
        } else {
            if (this.hasChildNodes()) {
                return this.childNodes.filter(el => el.contains(otherNode)).length !== 0
            } else {
                return false;
            }
        }
    }

    isEqualNode() {
        throw "抽象方法不能调用"
    }
};