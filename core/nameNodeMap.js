const { attribute } = require("../constant/reg");
const AttributeNode = require("./attributeNode")
module.exports = class NameNodeMap {
    constructor(attrStr) {
        this.list = [];
        const attrStrBak = attrStr.slice()
        let result;
        while ((result = attribute.exec(attrStrBak)) !== null) {
            this.list.push(new AttributeNode(result[1], result[2]))
        }
    }

    
    get length() {
        return this.list.length
    }
}
