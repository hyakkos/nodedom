const { NODETYPE } = require("../constant/node");
const BaseNode = require("./baseNode");
const ElementNode = require("./elementNode");

module.exports = class DocumentNode extends ElementNode{
    constructor(elemntStr){
        super(elemntStr, NODETYPE.DOCUMENT_NODE);
    }
}