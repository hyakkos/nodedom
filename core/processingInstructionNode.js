const { NODETYPE } = require("../constant/node");
const BaseNode = require("./baseNode");

module.exports = class ProcessingInstructionNode extends BaseNode{
    constructor(){
        super(NODETYPE.PROCESSING_INSTRUCTION_NODE)
    }
}