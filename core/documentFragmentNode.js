const { NODETYPE } = require("../constant/node");
const BaseNode = require("./baseNode");

module.exports = class DocumentFragmentNode extends BaseNode {
    constructor() {
        super(NODETYPE.DOCUMENT_FRAGMENT_NODE);
    }
}