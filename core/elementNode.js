const { NODETYPE } = require("../constant/node");
const { elementTag } = require("../constant/reg");
const BaseNode = require("./baseNode");
const NameNodeMap = require("./nameNodeMap");

module.exports = class ElementNode extends BaseNode {
    constructor(elemntStr, type = NODETYPE.ELEMENT_NODE) {
        super(type);
        this.outerHTML = elemntStr.trim();
        const result = this.outerHTML.match(elementTag);
        if (result && result.length) {
            this.tagName = result[1];
            if (result[2] && result[3].trim()) {
                this.attributes = new NameNodeMap(result[3].trim());
            }
            if (result[3] && result[3] === '/') {
                this.single = true;
                this.innerHTML = elemntStr.slice(result[0].length, elemntStr.lastIndexOf(`</${this.tagName}>`))
                this.parseChild()
            } else {
                this.single = false;
                this.innerHTML = null;
            }
        }
    }

    parseChild(){
        
    }

    get localName() {
        if (this.tagName) {
            return this.tagName.split(":").slice(-1)[0]
        }
        return ''
    }

    get prefix() {
        if (this.tagName) {
            return this.tagName.slice(0, - this.localName.length - 1)
        }
    }
}