const { NODETYPE } = require("../constant/node");
const BaseNode = require("./baseNode");

module.exports = class CDataSectionNode extends BaseNode{
    constructor(){
        super(NODETYPE.CDATA_SECTION_NODE);
    }
}