const { NODETYPE } = require("../constant/node");
const BaseNode = require("./baseNode");

module.exports = class CommentNode extends BaseNode {
    constructor(commentStr) {
        super(NODETYPE.COMMENT_NODE);
        this.value = commentStr
    }

    isEqualNode(otherNode) {
        return otherNode &&
            this.nodeType === otherNode.nodeType &&
            this.value !== otherNode.value
    }
}