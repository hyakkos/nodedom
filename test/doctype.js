const parseHTML = require("../core");

const doctypeMap = [
    `<!DOCTYPE html>`,
    `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">`,
    `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">`,
    `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">`,
    `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">`,
    ` `
]

let doc;
for (doctype of doctypeMap) {
    doc = new parseHTML(doctype);
    if (doc.doctype) {
        console.log(doc.doctype.valueOf())
    } else {
        console.log("未匹配到")
    }
}
