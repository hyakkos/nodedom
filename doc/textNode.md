# textNode 文本节点

## 继承自[baseNode](./baseNode.md)

## 自有属性

| 属性 | 描述 |
| - | - |
| value |  文本节点的值 |
| nodeName |  节点名称 #text |
