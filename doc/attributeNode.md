# attributeNode  属性节点

## 继承自[baseNode](./baseNode.md)

## 自有属性

| 属性 | 描述 |
| - | - |
| name |  属性的名称包含命名空间前缀的名字 |
| nodeName |  属性的名称包含命名空间前缀的名字 |
| value | 属性的值 |
| prefix | 返回指定标签属性的名字空间前缀，如果没有前缀则返回 null |
| localName | 返回一个属性限定名称的本名部分（去除命名空间前缀的名字）|
