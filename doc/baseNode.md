# baseNode 所有节点的基类

## 自有属性

| 属性 | 描述 |
| - | - |
| nodeType |  节点的类型 |
| childNodes | 节点的子节点的集合|
| firstChild | 节点的第一个子节点，如果节点无子节点，则返回 null|
| lastChild | 节点的最后一个子节点，如果节点无子节点，则返回 null|
| nextSibling | 当前节点的后一个兄弟节点，没有则返回null|
| previousSibling | 当前节点的前一个兄弟节点，没有则返回null|
| parentNode | 节点在 DOM 树中的父节点|

## 自有方法

| 名称 | 描述 |
| - | - |
| hasChildNodes |  节点是否函数子节点 |
| contains | 是否包含节点 节点自身可以包含自身|
| isEqualNode | 判断2个节点是否相同(此基类方法不能直接调用)|
