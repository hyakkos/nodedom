# doctypeNode 文档申明节点

## 继承自[baseNode](./baseNode.md)

## 自有属性

| 属性 | 描述 |
| - | - |
| name |  文档申明根元素 |
| nodeName |  节点名称等同于name |
| publicId | html4 专有 |
| systemId | html4 专有 |

## 备注

```xml
<!-- 内部申明 -->
<!DOCTYPE root-element [element-declarations]>
<!-- 外部申明 -->
<!DOCTYPE root-element SYSTEM "filename">

```

## 文档构建模块

1. 元素
2. 属性
3. 实体
4. PCDATA 被解析的字符数据
5. CDATA 不会被解析器解析的文本
